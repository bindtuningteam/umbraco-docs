**Important:** Your theme must be installed and applied to the site before uploading the demo content.

Much as an Umbraco starter kit, BindTuning themes are packed with demo content files that are easy to set up and are a great jump start for building content areas for your website. 

<a name="downloadthe.zipfile"></a>
### Download the .zip file 
Log in to your account at [bindtuning.com](http://bindtuning.com). Inside your admin dashboard click on *My Themes* and **select the theme**. 

Inside the theme's page, under documentation, click on the demo content link to download the .zip file.

<!--![uploadthedemocontent_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_1.png)--> 

<a name="unzipthefile"></a>
### Unzip the file

Before uploading the demo content into your website you will need to unzip the file. The file name is ***theme.UMB7.DemoContent.zip***.

After unzipping the file, **open the demo content file**(.txt). If you are using the Wide Master Template, then open the corresponding demo content file. In this case is DemoContentfor_Wide.txt.

Inside the .txt file you will find content for different zones, including the slider zone, footer, social zone, etc. If you want to add a Slider zone check our KB article on [Adding a slider in Umbraco](http://support.bindtuning.com/hc/en-us/articles/204448599-Adding-a-slider-in-Umbraco)).

<!--![uploadthedemocontent_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_2.png)--> 

**Copy the html code** for the content you want to add and move on to uploading the demo content.

![uploadthedemocontent_5](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_5.png) 


<a name="uploadingthedemocontent"></a>
### Uploading the demo content 
Which Umbraco version are you working with?

- [Version 7 (only MVC)](#version7)
- [Version 4-6](#version4-6) 

------

<a name="version7"></a>
####Version 7 (only MVC)
Follow these steps to add the demo content: 

1. **Login to your Umbraco admin area** (http://yourURL/umbraco); 
2. **Click on the *Settings* section** and **open the *Templates* folder**;
	
	![uploadthedemocontent_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_3.png) 
3. **Select the master template** where you want to add the demo content. 
4. On the *Template* tab, **paste the html code** from the txt file; 
5. **Enter the zones where you want to place the content**;

	![uploadthedemocontent_6](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_6.png) 
	
	**Note:** You can see which zones does your Master Template and theme include. Just open the Views and the Grid folder. Now click on the the Master that you are currently using. Once you have opened the file you will find all the zones that your theme and Master include on the code.

	![uploadthedemocontent_7](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_7.png) 
5.**Click Save**; All done! 

**Note:** **If you only want to use BindTuning demo content** on a particular Master, and the master is still inheriting content from one of Umbraco's starter kits, like [Fanoe](https://our.umbraco.org/projects/starter-kits/fanoe/), delete the tag `@CurrentPage.GetGridHtml("content", "fanoe")`. 

![uploadthedemocontent_8](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_8.png) 

------

<a name="version4-6"></a>
####Version 4-6
Follow these steps to add the demo content: 

1. **Login to your Umbraco admin area** (http://yourURL/umbraco); 
2. **Click on the *Settings* section** and **open the *Templates* folder**;
	
	![uploadthedemocontent_12](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_12.png) 
3. **Select the master template** where you want to add the demo content. 
4. **Paste the code** from the txt file; 
5. **Enter the following tags wrapping the code you just pasted: 

	`<asp:content ContentPlaceHolderID="*zonewhereyouareplacingthecontent*" runat="server">`
		
	`</asp:content>` 

	![uploadthedemocontent_14](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_14.png) 
	
	**Note:** You can see which zones does your Master Template and theme include. Just open the Views and the Grid folder. Now click on the the Master that you are currently using. Once you have opened the file you will find all the zones that your theme and Master include on the code.

	![uploadthedemocontent_13](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_13.png) 
5.**Click Save**; All done! 

------

<a name=uploadingthedemoimages"></a> 
###Uploading the demo images
If you are adding demo content that happens to include images sources on the code, you will need to upload those images into Umbraco. 

**Important:** All the images provided in the demo content folder are for demo purposes only.

Which Umbraco version are you working with:

- [Version 7 (only MVC)](#version7images)
- [Version 4-6](#version4-6images) 

------

<a name="version7images"></a>
####Version 7 (only MVC)
1. **Go your Umbraco admin area**; 
2. **Click on the *Media* section**; 
3. **Hover over the *Media* node** and you’ll see three dots ... , **click on the dots to see the menu**; 
2. **Click on the folder icon**, to create a folder for your images;
3. **Give it a name** and **click *Save***;  
4. **Click on *Click to upload***; 

	![uploadthedemocontent_9](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_9.png)
4. **Open the *theme*.UMB7.DemoContent file**;
2. **Open the *images* folder**. There you will find all the images that you were able to see on the theme's live preview at bindtuning.com;
	
	![uploadthedemocontent_10](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_10.png)
3. **Select the images** you want to upload to your folder;
4. Still inside your Umbraco admin area, **go to the *Settings* section**;
5. **Open the *Templates* folder** and **open the master** where you want to include the images;
6. **Replace the source on the code**. 
7. **Click *Save*.** 


![uploadthedemocontent_11](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_11.png)

Looking good! On to the next chapter. 

------

<a name="version4-6images"></a>
####Version 4-6
1. **Go your Umbraco admin area**; 
2. **Click on the *Media* section**; 

	![uploadthedemocontent_15](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_15.png)

3. Click on the upload icon;
4. **Open the *theme*.UMB7.DemoContent file** and **open the *images* folder**. There you will find all the images that you were able to see on the theme's live preview at bindtuning.com;

	![uploadthedemocontent_16](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_16.png)
4. Still inside your Umbraco admin area, **go to the *Settings* section**;
5. **Open the *Templates* folder** and **open the master** where you want to include the images;
6. **Replace the source on the code**. 
7. **Click *Save*.** 


![uploadthedemocontent_18](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_18.png)
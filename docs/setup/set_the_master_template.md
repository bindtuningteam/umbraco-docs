BindTuning themes include a variety of responsive master templates that you can use and select for different parts of your site.   


<a name="version7"></a>
###Version 7 (MVC only)
To apply one of BindTuning themes master templates follow these steps:

1. In your admin area ((http://yourURL/umbraco), go to the *Settings* section;

2. **Open the *Templates* folder**;

	![uploadthedemocontent_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uploadthedemocontent_3.png) 

3. **Select the template** where you want to apply BindTuning master template. 
	
	![setthemasterpage_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/setthemasterpage_2.png) 
 
4. **Switch to the *Properties* tab**;

5. On the Master template box, **choose which master you want to apply** to the template; BindTuning master templates have the following terminology: ***yourthemename Nameofthemastertemplate***; 

	![setthemasterpage_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/setthemasterpage_3.png) 

6. **Click *Save***. All done!

-------

<a name="version4-6"></a>
###Version 4-6
To apply one of BindTuning themes master templates follow these steps:

1. In your admin area ((http://yourURL/umbraco), go to the *Settings* section;

	![setthemasterpage_7](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/setthemasterpage_7.png) 

2. **Open the *Templates* folder**;

3. **Select the template** where you want to apply BindTuning master template. 
	
	![setthemasterpage_8](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/setthemasterpage_8.png) 
 
5. On the Master template box, **choose which master you want to apply** to the template; BindTuning master templates have the following terminology: ***yourthemename Nameofthemastertemplate***; 

	![setthemasterpage_9](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/setthemasterpage_9.png) 

6. **Click on the *Save*** icon. All done!





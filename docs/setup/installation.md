**Important:** If you are installing a trial theme, you might get a warning/conflict with a dll file. Continue the installing ignoring the warning/conflict.

**Important:** If you already have any of BindTuning themes installed, you might get a few file conflicts. Ignore the conflicts and continue the Installation process.

<a name="version7"></a>
###Version 7 (MVC only)

####Installing the theme
Now let's get down to business. Follow these steps to install your theme: 

1. **Login to Umbraco admin area**;

2. **Click on the Developer section**;

	![install_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_2.png) 

3. Open the folder Packages and **click on *Install local package***;

	![install_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_3.png) 

4. Click on *Choose File* and **search for your theme package**, *yourthemename*.UMB7.zip;

5. **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;
	
	![install_4](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_4.png) 

6. **Check “Accept license”** and **click on *Install Package*** to install the theme;
	
	![install_5](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_5.png) 

7. And that's it. You should get a message saying: *view installed package* or *view package website*. 

####Finishing off
Before wrapping up the installation lets check if the theme was successfully installed and activated.

Still inside the ***Developer*** section, **open the *Packages* folder** and **open the *Installed packages* folder**. If your theme appears listed it was successfully installed.

![install_7](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_7.png) 

**Success**: You've now successfully installed your theme.

-----

<a name="version4-6"></a>
###Version 4-6

####Installing the theme
Now lets get down to business. Follow these steps to install your theme: 

1. **Login to Umbraco admin area**;
2. **Click on the Developer section**;

	![install_8](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_8.png) 

3. Open the folder Packages and **click on *Install local package***;

	![install_9](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_9.png) 

4. Click on *Choose File* and **search for your theme package**, *yourthemename*.UMB7.zip;
5. **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;
	
	![install_10](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_10.png) 

6. **Check “Accept license”** and **click on *Install Package*** to install the theme;
	
	![install_11](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_11.png) 

7. And that's it. You should get a message saying: *view installed package* or *view package website*. 


####Finishing off
Before wrapping up the installation lets check if the theme was successfully installed and activated.

Still inside the ***Developer*** section, **open the *Packages* folder** and **open the *Installed packages* folder**. If your theme appears listed it was successfully installed.

![install_12](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_12.png) 

**Success**: You've now successfully installed your theme.
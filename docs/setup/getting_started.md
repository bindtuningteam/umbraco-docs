<a name="downloadthetheme"></a>
### Download the theme ###
Ready to get started? First you need to download your theme's zip file from bindtuning.com. **Access your theme details page** and **click on the *Download* button** to download your theme. 

<!--![gettingstarted_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/gettingstarted_1.png)--> 

<a name="insidethe.zipfile"></a>
### Inside the .zip file ###
Let's take a look at what the zip file contains. Inside your theme package you will find two folders, one for Containers and the other one with Skin Files.

- ***Containers*** 
	
	Inside the *Containers* folder you can find a number of html files for different containers. You will use containers later on for wrapping your content. 

	<!--![gettingstarted_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/gettingstarted_3.png)-->  	

- ***yourthemename*SkinFiles**

	Inside the *yourthemename*SkinFiles folder you will find JavaScript files, PNG files, GIF files, CSS files, etc.

	<!--![gettingstarted_4](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/gettingstarted_4.png)-->  


Now let's get your website rocking! First thing on the list is go through all the [Requirements](https://umbraco.readthedocs.io/en/latest/setup/requirements/) before installing the theme. 

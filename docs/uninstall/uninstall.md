<a name="version7"></a>
###Version 7 (only MVC)
####Change the Template
Step 1 of uninstalling any BindTuning theme from Umbraco is to change the Master Template in case you are using any of the theme's templates. 

1. **Open your Umbraco admin area**; 
2. On the left menu, **click on *Settings***; 

	![uninstall_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_1.png)
	
3. **Open the *Templates* folder**;
4. **Select the master template** you have activated and is using one of BindTuning templates. All BindTuning templates have this kind of terminology: ***yourthemename NameoftheMasterTemplate***. 
5. **Switch to the *Properties* tab**;
6. On the Master template, **choose *none***;

	![uninstall_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_2.png)
	
7. **Click *Save***. Done! 

**Repeat this process for all the master templates that are using one of BindTuning templates.**

Lets move on to deleting the theme.

####Deleting the theme

1. **Open your Umbraco admin area**; 
2. On the left menu, **click on *Developer***; 
3. **Open the *Packages* folder** and **open *Installed packages* folder**;

	![uninstall_4](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_4.png)
4. **Select your theme package**; 
5. **Switch to the *Uninstall package* tab**;

	![uninstall_5](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_5.png)
6. Go through all the templates and files to make sure every file is selected;
7. **Click on *Confirm uninstall***. All done!



####Finishing off
Only a few more final steps to completely remove and uninstall your theme.

Open the server where your Umbraco site is installed. Now open these folders and delete any remaining files associated with the theme. 

![uninstall_6](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_6.png)

- [Your Umbraco root]/**css**/**MyTheme** folder and the respective css file
- [Your Umbraco root]/**views** [delete all the masters associated with the theme]
- [Your Umbraco root]/**scripts**/**MyTheme**
- [Your Umbraco root]/**views**/**Partials**/***yourthemename*Breadcrumb.cshtml**
- [Your Umbraco root]/**views**/**Partials**/***yourthemename*Navigation.cshtml**


**Success:** You've now successfully removed and uninstalled your theme.


<a name="version4-6"></a>
##Version 4-6
###Change the Template
Step 1 of uninstalling any BindTuning theme from Umbraco is to change the Master Template in case you are using any of the theme's templates. 

1. **Open your Umbraco admin area**; 
2. **Click on *Settings***; 

	![uninstall_6](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_6.png)
3. **Open the *Templates* folder**;
4. **Select the master template** you have activated and is using one of BindTuning templates. All BindTuning templates have this kind of terminology: ***yourthemename NameoftheMasterTemplate***. 
6. On the Master template, **choose *none***;

	![uninstall_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_2.png)
7. **Click on the *Save* icon**. Done! 

**Repeat this process for all the master templates that are using one of BindTuning templates.**

Lets move on to deleting the theme.

###Deleting the theme

1. **Open your Umbraco admin area**; 
2. **Click on *Developer***; 
3. O**pen the *Packages* folder** and **open *Installed packages* folder**;
4. **Select your theme package**; 
5. **Click on *Uninstall package***;

	![uninstall_8](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_8.png)
6. Go through all the templates and files to make sure every file is selected;

	![uninstall_9](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_9.png)
7. **Click on *Confirm uninstall***.
8. You should get a message saying "The package was successfully uninstalled".
	
	![uninstall_10](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_10.png)

###Finishing off
Only a few more final steps to completely remove and uninstall your theme.

Open the server where your Umbraco site is installed. Now open these folders and delete any remaining files associated with the theme. 

![uninstall_6](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/uninstall_6.png)

- [Your Umbraco root]/**css**/**MyTheme** folder and the respective css file
- [Your Umbraco root]/**views** [delete all the masters associated with the theme]
- [Your Umbraco root]/**scripts**/**MyTheme**
- [Your Umbraco root]/**views**/**Partials**/***yourthemename*Breadcrumb.cshtml**
- [Your Umbraco root]/**views**/**Partials**/***yourthemename*Navigation.cshtml**


**Success:** You've now successfully removed and uninstalled your theme.